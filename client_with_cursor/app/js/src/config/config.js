//*******************************************
//  Global System Configurations
//*******************************************


var GlobalConfig = (function () {
  var ConnectToServer = true; //true/false 

  var DeviceType = "mobile";

  var serverIP = '127.0.0.1';

  var Connections = {
    Sockets: `ws:${serverIP}:8123`, // URL of socket server
    Http : `http://${serverIP}:8760` // URL of http - rest server
  };

  var Pages = {
    InitialPage: 'page1'     // initial page to display
  };


  return {
    ConnectToServer: ConnectToServer,
    Connections: Connections,
    Pages: Pages,
    DeviceType: DeviceType
  }
})();
