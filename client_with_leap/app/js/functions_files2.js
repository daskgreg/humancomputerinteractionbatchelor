function previewPDF(callerId){
	var pdf_obj;

	pdf_obj = document.createElement('OBJECT');
	pdf_obj.id = "preview-" + callerId;
	pdf_obj.setAttribute("data", "./data/file.pdf");
	pdf_obj.setAttribute("width", ".1600");
	pdf_obj.setAttribute("height", "1600");
	
	document.getElementById('container-pdf').appendChild(pdf_obj);
}

Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
};
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
};