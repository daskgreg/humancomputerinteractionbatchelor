//*******************************************
//  SOCKETS CONTROLLER
//*******************************************
var Sockets = (function () {


  function HandleMessages(e) {
    var data = jQuery.parseJSON(e.data);

    var msgType = data.type;

    /* rotation: 27791
      button & proximity: 315555
      id:6 - bottom right-right
      id:7 bottom-right-left
      id:0 proximity
      horizontal:65462
      vertical: 65464*/
    console.log(data.message);
    /*PHIDGETS ROTATION HANDLER*/
    if (data.message.serialNumber == 27791) { /*Rotation*/
      /*if the page I want to change is the one being shown*/
      if (document.getElementById("files1Page").className.includes('pt-page-current')) {
        var vol = data.message.actualValue;
        var final_volume = vol / 1000;
        console.log(final_volume);
        document.getElementById('video1').volume = final_volume;
      }
    }

    if (data.message.serialNumber == 315555) { /*buttons*/
      if (data.message.id == 6) {
        if (document.getElementById("files1Page").className.includes('pt-page-current')) {
          document.getElementById('video1').play();
        }
      }
      if (data.message.id == 7) {
        if (document.getElementById("files1Page").className.includes('pt-page-current')) {
          document.getElementById('video1').pause();
        }
      }
      if (data.message.id == 0) {

      }
    }

    if (data.message.serialNumber == 65464) {
      if (document.getElementById("createMeetingPage").className.includes('pt-page-current')) {
       /* var height=document.getElementById('invite').clientHeight;
        var input=data.message.actualValue;
        input = input/10;
        var scrollValue = height*(input/100);
        
        console.log(scrollValue);
        document.getElementsByClassName('tabcontent')[0].scrollTop( scrollValue);*/
      }
    }
    if (data.message.serialNumber == 65462) {
      if (document.getElementById("files1Page").className.includes('pt-page-current')) {
        var length = document.getElementById('video1').duration;
        var input = data.message.actualValue;
        input = input / 10;
        length = length * (input / 100);
        document.getElementById('video1').currentTime = length;
      }
    }
    //console.log(data.message.serialNumber);
    /*GESTURES HANDLER*/
    if (msgType.includes('kinect/gesture')) {

      if (data.message.gestureType == 'SwipeLeft') {
        if (document.getElementById('presentationSlide1Page').className.includes('pt-page-current')) {
          PageTransitions.goToPage(1, 'presentationSlide2Page')
        } else if (document.getElementById('presentationSlide2Page').className.includes('pt-page-current')) {
          PageTransitions.goToPage(1, 'presentationSlide3Page')
        } else if (document.getElementById('presentationSlide3Page').className.includes('pt-page-current')) {
          PageTransitions.goToPage(1, 'presentationSlide4Page')
        } else {
          //could print an error
        }

        if (document.getElementById('photoViewer').className.includes('pt-current-page')) {
          document.getElementById('rightBtn_photoViewer').click();
        }
      }

      if (data.message.gestureType == 'SwipeRight') {
        if (document.getElementById('presentationSlide4Page').className.includes('pt-page-current')) {
          PageTransitions.goToPage(1, 'presentationSlide3Page')
        } else if (document.getElementById('presentationSlide3Page').className.includes('pt-page-current')) {
          PageTransitions.goToPage(1, 'presentationSlide2Page')
        } else if (document.getElementById('presentationSlide2Page').className.includes('pt-page-current')) {
          PageTransitions.goToPage(1, 'presentationSlide1Page')
        } else {
          //could print an error
        }

        if (document.getElementById('photoViewer').className.includes('pt-current-page')) {
          document.getElementById('leftBtn_photoViewer').click();
        }
      }
    }

    /*VOICE COMMANDS HANDLER*/
    if (msgType.includes('kinect/speech')) {
      if (data.message.speechValue == 'NEXTSLIDE') {
        /******************************PRESENTATION***************************************/
        if (document.getElementById('presentationSlide1Page').className.includes('pt-page-current')) {
          PageTransitions.goToPage(1, 'presentationSlide2Page')
        } else if (document.getElementById('presentationSlide2Page').className.includes('pt-page-current')) {
          PageTransitions.goToPage(1, 'presentationSlide3Page')
        } else if (document.getElementById('presentationSlide3Page').className.includes('pt-page-current')) {
          PageTransitions.goToPage(1, 'presentationSlide4Page')
        } else {
          //could print an error
        }
        /**************************END PRESENTATION***************************************/

        if (document.getElementById('photoViewerPage').className.includes('pt-current-page')) {

          document.getElementById('rightBtn_photoViewer').click();
        }
      }

      if (data.message.speechValue == 'PREVSLIDE') {
        /******************************PRESENTATION***************************************/
        if (document.getElementById('presentationSlide4Page').className.includes('pt-page-current')) {
          PageTransitions.goToPage(1, 'presentationSlide3Page')
        } else if (document.getElementById('presentationSlide3Page').className.includes('pt-page-current')) {
          PageTransitions.goToPage(1, 'presentationSlide2Page')
        } else if (document.getElementById('presentationSlide2Page').className.includes('pt-page-current')) {
          PageTransitions.goToPage(1, 'presentationSlide1Page')
        } else {
          //could print an error
        }
        /**************************END PRESENTATION***************************************/

        if (document.getElementById('photoViewer').className.includes('pt-current-page')) {
          document.getElementById('leftBtn_photoViewer').click();
        }
      }

      if (data.message.speechValue == 'START_PRESENTATION') {
        if (document.getElementById('meetingTimePage').className.includes('pt-page-current')) {
          PageTransitions.goToPage(1, 'presentationSlide1Page');
        }
      }

      if (data.message.speechValue == "EXIT") {
        if (document.getElementById('photoViewerPage').className.includes('pt-page-current')) {
          PageTransitions.goToPage(1, 'meetingTimePage');
        }

        if (document.getElementById('presentationSlide1Page').className.includes('pt-page-current') ||
          document.getElementById('presentationSlide2Page').className.includes('pt-page-current') ||
          document.getElementById('presentationSlide3Page').className.includes('pt-page-current') ||
          document.getElementById('presentationSlide4Page').className.includes('pt-page-current')) {
          PageTransitions.goToPage(1, 'meetingTimePage');
        }
      }
    }

    if (msgType.includes('helloWorld')) {
      HelloWorldManager.DataHandler(data);
    }

  }



  class WSConnection {

    constructor() {
      this.socketConnection = null;
    }

    /**
     * Connect to socket server
     */
    Connect() {
      this.socketConnection = new WebSocket(GlobalConfig.Connections.Sockets, GlobalConfig.DeviceType);

      this.socketConnection.onopen = function () {
        console.info("Sockets connected!");
      };

      this.socketConnection.onerror = function (error) {
        console.info('Sockets Error: ' + error);
      };


      this.socketConnection.onmessage = HandleMessages;
    }

    /**
     * Disconnect from socket server
     */
    Disconnect() {
      this.socketConnection.disconnect();
      console.info('Sockets disconnected!');
    }

    /**
     * Send message to socket server
     * 
     * @param {string} type - Type of message
     * @param {any} msg - Object to send as message  
     */
    SendMessage(type, msg) {
      console.log("dgdgdg");
      this.socketConnection.send(JSON.stringify({
        type: type,
        message: msg
      }));
    }

  }

  return new WSConnection();
})();
