
var mainManager = function () {

    /**
    * A class to manage the messages received from helloWorld
    */
    class main {
        /**
         * 
         * @param {object} data 
         * @param {} data.type 
         * @param {} data.message 
         */
        DataHandler(data) {
            
            if(data.type == "kinect/speech"){
                this.mainMessage(data.message);
            }

        }

        mainMessage(message) {
            $('#welcome-text').text(message);
            document.getElementById('welcome-text').innerText = message;
        }
    }

    return new main();
}();