
var HelloWorldManager = function () {

    /**
    * A class to manage the messages received from helloWorld
    */
    class HelloWorld {
        /**
         * 
         * @param {object} data 
         * @param {} data.type 
         * @param {} data.message 
         */
        DataHandler(data) {
            if (data.type === "helloWorld") {
                this.HelloMessage(data.message);
            }
            if(data.type == "kinect/speech"){
                this.HelloMessage(data.message);
            }
			if(data.type == "nextslide"){
			}

        }

        HelloMessage(message) {
            $('#welcome-text').text(message);
        }
    }

    return new HelloWorld();
}();