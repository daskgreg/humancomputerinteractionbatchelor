var current = "none";

$(document).ready(function () {
  //---------------------------------------
  //#region Demo Button Clicks

  $('.demoBtn').click(function () {
    $(this).toggleClass('checked')
  });



  /*MENU*/
  $('.meetingBtn').click(function () {
    PageTransitions.goToPage(1, 'meetingPage');
    current = "meetingPage";
  });

  $('.filesBtn').click(function () {
    PageTransitions.goToPage(1, 'files1Page');
    current = "files1Page";
  });

  $('.calendarBtn').click(function () {
    PageTransitions.goToPage(1, 'calendarPage');
    current = "calendarPage";
  });

  $('.settingsBtn').click(function () {
    PageTransitions.goToPage(1, 'settingsPage');
    current = "settingsPage";
  });
  /* END MENU*/

  /*MEETING*/
  $('.pastMeetingsBtn').click(function () {
    PageTransitions.goToPage(1, 'pastMeetingsPage');
    current = "pastMeetingsPage";
  });
  /*SLIDE 1*/
  $('.startMeetingBtn').click(function () {
    PageTransitions.goToPage(1, 'presentationSlide1Page');
    current = "presentationSlide1Page";
    showContainer();
  });
  /*END SLIDE 1*/
  /*SLIDE 2*/
  $('.slide1to2Btn').click(function () {
    PageTransitions.goToPage(1, 'presentationSlide2Page');
    current = "presentationSlide3Page";
    showContainer();
  });

  $('.slide2to1Btn').click(function () {
    PageTransitions.goToPage(2, 'presentationSlide1Page');
    current = "presentationSlide2Page";
    showContainer();
  });
  /*ENDSLIDE 2*/
  /*SLIDE 3*/
  $('.slide2to3Btn').click(function () {
    PageTransitions.goToPage(1, 'presentationSlide3Page');
    current = "presentationSlide4Page";
    showContainer();
  });

  $('.slide3to2Btn').click(function () {
    PageTransitions.goToPage(2, 'presentationSlide2Page');
    current = "presentationSlide3Page";
    showContainer();
  });
  /*END SLIDE 3*/
  /*SLIDE 4*/
  $('.slide3to4Btn').click(function () {
    PageTransitions.goToPage(1, 'presentationSlide4Page');
    current = "presentationSlide3Page";
    showContainer();
  });

  $('.slide4to3Btn').click(function () {
    PageTransitions.goToPage(2, 'presentationSlide3Page');
    current = "presentationSlide1Page";
    showContainer();
  });
  /*END SLIDE 4*/
  /*CLOSE*/
  $('.closeBtn').click(function () {
    PageTransitions.goToPage(1, 'meetingTimePage');
    showContainer();
  });
  /*END CLOSE*/
  /*END OF MEETING*/

  /*LOAD MEETING*/
  $('.loadBtn').click(function () {
    // Tip: try other integers [1-67] at PageTransitions.goToPage function
    // and see different animations on changing pages
    PageTransitions.goToPage(1, 'upload2Page');
    showContainer();
  });

  $('.iMeetMeetingsBtn').click(function () {
    PageTransitions.goToPage(1, 'upload2Page');
  });

  $('.myMeetingsDetailBtn').click(function () {
    PageTransitions.goToPage(1, 'createMeetingPage');
    current = "createMeetingPage";
    showContainer();
  });

  $('.startLoadedMeetingBtn').click(function () {
    PageTransitions.goToPage(1, 'meetingTimePage');
    current = "meetingTimePage";
    showContainer();
  });

  $('.myMeetingsDetailBtn').click(function () {
    document.getElementById('titleBox').innerText = name;
    PageTransitions.goToPage(1, 'createMeetingPage');
    current = "createMeetingPage";
    showContainer();
  });

  $('.myMeetingsStartBtn').click(function () {
    PageTransitions.goToPage(1, 'meetingTimePage');
    current = "meetingTimePage";
    showContainer();
  });
  /*END LOAD MEETING*/

  /*ALL MEETINGS*/
  $('.companyMeetingsBtn').click(function () {
    PageTransitions.goToPage(1, 'allMeetingsPage');
    current = "allMeetingsPage";
    showContainer();
  });

  $('.myMeetingsBtn').click(function () {
    PageTransitions.goToPage(1, 'loadMeetingPage');
    current = "loadMeetingPage";
    showContainer();
  });

  $('.photoMeetingTimeBtn').click(function () {
    PageTransitions.goToPage(1, 'photoViewerPage');
    current = "photoViewerPage";
    showContainer();
    photo_viewer(0);
  });

  $('.closePhotoViewerBtn').click(function () {
    PageTransitions.goToPage(1, 'meetingTimePage');
    current = "meetingTimePage";
    showContainer();
    photo_viewer(0);
  });
  /*END ALL MEETINGS*/

  /*FILES*/
  $('.files1Btn').click(function () {
    PageTransitions.goToPage(1, 'files1Page');
    current = "files1Page";
    showContainer();
  });

  $('.files2Btn').click(function () {
    PageTransitions.goToPage(1, 'files2Page');
    current = "files2Page";
    showContainer();
  });

  $('.files3Btn').click(function () {
    PageTransitions.goToPage(1, 'files3Page');
    current = "files3Page";
    showContainer();
  });
  /*END FILES*/

  $('.backBtn').click(function () {
    PageTransitions.goToPage(1, current);
    showContainer();
  });

  $('.demoBtn2').click(function () {
    PageTransitions.goToPage(2, 'dashboardPage');
  });

  $('.startBtn').click(function () {
    PageTransitions.goToPage(1, 'presentation1Page');
    current = "meetingPage";
  });
  
  

  //#endregion
  //---------------------------------------
});

function showContainer() {
  console.log(current);
  if (!document.getElementById('container')) return;
  if (current == "createMeetingPage") {
    console.log("visible");
    document.getElementById('container').style.visibility = 'visible';
  } else {
    console.log("hidden");
    document.getElementById('container').style.visibility = 'hidden';
  }
}
