var saved=0;
function opentab(evt, tab) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tab).style.display = "block";
    evt.currentTarget.className += " active";
}

window.onload = function () {
    document.getElementById('inv-btn').click();
	addButton("invite");
	addButton("suggested");	
	
	$(document).ready(function(){
	$.getJSON("/app/data/employees.json", function(json) {
		for(var i=0; i<30; i++){
			var inv_id = "invite-member-btn-" + i.toString();
			var sug_id = "suggested-member-btn-" + i.toString();
			console.log(inv_id);
			document.getElementById(inv_id).innerText = json[i].name +" "+json[i].position;
			document.getElementById(inv_id).value = json[i].name +" "+json[i].position;
			console.log(inv_id);
		}
	});
});
	
}

var emplyees_count=0;
var members_count=0;

function addButton(selection){
	var btn, list_obj;
	
	for(var i=0; i<30; i++){
		list_obj=document.createElement('LI');
		list_obj.id=selection.toString() + "list-obj" + i.toString();//list-obj1 est.
		
		btn=document.createElement('BUTTON');
		btn.id=selection.toString() + "-member-btn-" + i.toString();
		btn.className="member-btn";
		
		document.getElementById(selection.toString() + '-members-list').appendChild(list_obj);//add li to list
		document.getElementById(selection.toString() + "list-obj" + i.toString()).appendChild(btn);//add button to list
		
		document.getElementById(selection.toString() + "-member-btn-" + i.toString()).setAttribute('onclick','addMember(this.id)');
		 
		

	}
}

function addMember(id){//selects member to be added in the meeting
	var div, img_name, container, name_container,sheet, name_sheet;
	var img_no = members_count+1;
	
	if((document.getElementById(id).style.backgroundColor!="rgb(84, 205, 167)")){//member already selected
	
		if(!document.getElementById('container')){
			container=document.createElement('div');
			container.id='container';
			container.className="container-class";
			document.getElementsByTagName('body')[0].appendChild(container);
		}
		
		if(members_count == 7){return;}//max 7 members
		
		//div for members names
		name_container=document.createElement('div');
		name_container.id='name-container' + members_count.toString();
		name_container.className="name-container-class";
		/* document.getElementsByTagName('body')[0].appendChild(name_container); */
		
		
		
		div=document.createElement('div');
		div.id='member' + members_count.toString();//div name = member 1,2,3
		
		img_name=document.createElement('div');
		img_name.id='img_name' + members_count.toString();
		
		document.getElementById('container').appendChild(div);
		
		document.getElementById('member' + members_count.toString()).appendChild(name_container);
		
		document.getElementById('name-container' + members_count.toString()).innerText = document.getElementById(id).innerText;
		
		sheet = document.createElement('style');
		sheet.type="text/css";
		sheet.innerHTML = '#member' + members_count.toString() + "{position:relative;  width:150px; height:150px; border-radius:100px;  border: 3px solid rgba(255,255,255,0.4); display:inline-block; margin:0 15px; font-family:myriad-pro-reg; font-size:19px; color:white;}";
		document.getElementById('member' + members_count.toString()).style.backgroundImage='url("./app/images/loading/face' + img_no.toString() + '.jpg")';
		/* document.getElementById('img_name' + members_count.toString()).innerText = document.getElementById(id).innerText; */
		
		name_sheet = document.createElement('style');
		name_sheet.type="text/css";
		name_sheet.innerHTML = '#name-container' + members_count.toString() + "{position:absolute; top:150px;}";
		
		document.getElementsByTagName('head')[0].appendChild(sheet);
		document.getElementsByTagName('head')[0].appendChild(name_sheet);
		
		members_count++;
		makeGreenBg(id);
	}
}

function membersSearch(query){
	console.log(query);
	var matches_no=0;
	if(query === "" || query.length < 3){
		console.log("KappaROss");
		for(var i=0; i<30; i++){
			document.getElementById('invite-member-btn-' + i.toString()).style.visibility = "visible";
			document.getElementById('invitelist-obj' + i.toString()).style.visibility = "visible";
		}
		//restore
	}else{
		//delete
		for(var i=0; i<30; i++){
			document.getElementById('invite-member-btn-' + i.toString()).style.visibility = "hidden";
			document.getElementById('invitelist-obj' + i.toString()).style.visibility = "hidden";
		}
		var j=0;
		for(var i=0; i<30; i++){
			var btn_value = document.getElementById('invite-member-btn-' + i.toString()).value;
			var btn_value_up = btn_value.toUpperCase();
			if(btn_value_up.includes(query.toUpperCase())){//oso exw matches
				document.getElementById('invite-member-btn-' + j.toString()).style.visibility = "visible";
				document.getElementById('invitelist-obj' + j.toString()).style.visibility = "visible";
				document.getElementById('invite-member-btn-' + j.toString()).innerText = btn_value;//swap 
				console.log(btn_value);
				//swap(i,j);
				j++;
			}
		}
	}
}

function swap(i,j){
	var name = document.getElementById('invite-member-btn-' + j.toString()).innerText;
	var bgcolor = document.getElementById('invite-member-btn-' + j.toString()).style.backgroundColor;
	console.log(name, bgcolor, i, j);
	if(i == j) return;
	
	document.getElementById('invite-member-btn-' + j.toString()).innerText = document.getElementById('invite-member-btn-' + i.toString()).innerText;
	document.getElementById('invite-member-btn-' + j.toString()).style.backgroundColor = document.getElementById('invite-member-btn-' + i.toString()).style.backgroundColor;
	
	document.getElementById('invite-member-btn-' + i.toString()).innerText = name;
	document.getElementById('invite-member-btn-' + i.toString()).style.backgroundColor = bgcolor;
	
	document.getElementById('invite-member-btn-' + j.toString()).style.visibility = "visible";
	console.log(name, bgcolor, i, j);
}

function saver(){
	//keeps title, description, and attendees' id in a text file, kai h panagia mazi mas
	saved=1;
	alert("Saving Meeting, keeps title, description, and attendees' id in a text file")
}

function createMeetingBack(){
		PageTransitions.goToPage(2, 'meetingPage');
		document.getElementById('container').style.visibility = 'hidden';
}

function deleteSession() {
	var ask = window.confirm("Are you sure you want to delete this session?");
    if (ask) {
        window.alert("This session was successfully deleted.");
		
	 	PageTransitions.goToPage(2, 'meetingPage');
		document.getElementById('container').remove();
		
		for(var i=0; i<30; i++){
			makeTransparentBg('invite-member-btn-'+i);
		}
		saved=0;
    }
}

function makeGreenBg(btn) {
    var property = document.getElementById(btn);
	property.style.backgroundColor = "#54cda7";
}

function makeTransparentBg(btn) {
    var property = document.getElementById(btn);
	property.style.backgroundColor = "Transparent";
}

Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
};
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
};