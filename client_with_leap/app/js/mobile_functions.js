$(document).ready(function () {
    $('.menuSidebar').hide();
	$('.emptyPart').hide();
    $('.openMenuBtn').click(function () {
        $('.menuTopbar').hide();
        $('.menuSidebar').show();   
		$('.emptyPart').show();
    });
	
	$('.emptyPart').click(function(){
		$('.menuTopbar').show();
        $('.menuSidebar').hide();
		$('.emptyPart').hide();
	});
	
	$('.loginBtn').click(function () {
		PageTransitions.goToPage(1, 'meetingsPage');
	});
	/****************MENU**************/
	$('.meetingsBtn').click(function(){
		$('.menuTopbar').show();
        $('.menuSidebar').hide();
		$('.emptyPart').hide();
		PageTransitions.goToPage(1, 'meetingsPage');
	});
	$('.filesBtn').click(function(){
		$('.menuTopbar').show();
        $('.menuSidebar').hide();
		$('.emptyPart').hide();
		PageTransitions.goToPage(1, 'filesPage');
	});
	$('.calendarBtn').click(function(){
		$('.menuTopbar').show();
        $('.menuSidebar').hide();
		$('.emptyPart').hide();
		PageTransitions.goToPage(1, 'calendarPage');
	});
	$('.settingsBtn').click(function(){
		$('.menuTopbar').show();
        $('.menuSidebar').hide();
		$('.emptyPart').hide();
		PageTransitions.goToPage(1, 'settingsPage');
	});
	/**********************************/
	$('.liveMeetingBtn').click(function(){
		$('.menuTopbar').show();
        $('.menuSidebar').hide();
		$('.emptyPart').hide();
		PageTransitions.goToPage(1, 'beforeMeetingPage');
	});
});
