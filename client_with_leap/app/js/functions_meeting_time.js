var image, modal;
var imgCnt=1;

function photo_viewer(param){
	var div, leftBtn, rightBtn, closeBtn;
	var sheet, imageSheet, leftBtnSheet, rightBtnSheet, closeBtnSheet,btnClass;
	
	
		controlPage('hidden');
	
	if(document.getElementById('photo_viewer_container').style.visibility == 'hidden'){
		document.getElementById('photo_viewer_container').style.visibility = 'visible';
		return;
	}
	
	
	
	
	if(param == 0){
		if(image){
			return;
		}
		/*div = document.createElement('div');
		div.id = "photo_viewer_container";*/

		sheet = document.createElement('style');
		sheet.type = 'text/css';
		sheet.innerHTML = "#photo_viewer_container{width:100%; height:1000px; background:rgba(0,0,0,0.7);}";

		image = document.createElement('img');
		image.id = 'imageDiv'
		image.setAttribute('src', './images/loading/face'+imgCnt.toString()+'.jpg');

		leftBtn = document.createElement('BUTTON');
		leftBtn.id='leftBtn_photoViewer';
		leftBtn.innerText = '<';
		leftBtn.className = 'photoViewerBtn';
		leftBtn.setAttribute("onclick", "prevImg('image')")

		rightBtn = document.createElement('BUTTON');
		rightBtn.id='rightBtn_photoViewer';
		rightBtn.innerText = '>';
		rightBtn.className = 'photoViewerBtn';
		rightBtn.setAttribute("onclick", "nextImg('image')")

		leftBtnSheet = document.createElement('style');
		leftBtnSheet.type = 'text/css';
		leftBtnSheet.innerText = "#leftBtn_photoViewer{position:absolute; top:1085px;right:860px;}"

		rightBtnSheet = document.createElement('style');
		rightBtnSheet.type = 'text/css';
		rightBtnSheet.innerText = "#rightBtn_photoViewer{position:absolute; top:1085px;right:740px;}"

		btnClass = document.createElement('style');
		btnClass.type = 'text/css';
		btnClass.innerText = ".photoViewerBtn{width:80px; height:80px;border:1px solid white; border-radius:50px;}";
		
		closeBtn = document.createElement('BUTTON');
		closeBtn.id = 'closePhotoViewer';
		closeBtn.innerText = 'X';
		closeBtn.className = 'photoViewerBtn closePhotoViewerBtn';
		closeBtn.setAttribute("onclick", "closeImg()");
		
		closeBtnSheet = document.createElement('style');
		closeBtnSheet.type = 'text/css';
		closeBtnSheet.innerText = "#closePhotoViewer{background-color: #cc5454;position:absolute; top:1085px; right:980}"

		imageSheet = document.createElement('sheet');
		imageSheet.type = 'text/css';
		imageSheet.innerText = "imageDiv{position:absolute; top:100px}";

		//document.body.appendChild(div);
		document.getElementById('photo_viewer_container').appendChild(image);
		document.getElementById('photo_viewer_container').appendChild(leftBtn);
		document.getElementById('photo_viewer_container').appendChild(rightBtn);
		document.getElementById('photo_viewer_container').appendChild(closeBtn);
		document.getElementsByTagName('head')[0].appendChild(sheet);
		document.getElementsByTagName('head')[0].appendChild(leftBtnSheet);
		document.getElementsByTagName('head')[0].appendChild(rightBtnSheet);
		document.getElementsByTagName('head')[0].appendChild(closeBtnSheet);
		document.getElementsByTagName('head')[0].appendChild(btnClass);
		document.getElementsByTagName('head')[0].appendChild(imageSheet);
		
		
		
	}else if(param == 1){
		image.remove();
		image = document.createElement('img');
		image.setAttribute('src', './app/images/loading/face'+imgCnt.toString()+'.jpg');
		
		document.getElementById('photo_viewer_container').appendChild(image);
	}else if(param == 2){
		document.getElementById('photo_viewer_container').style.visibility = 'hidden';
		//controlPage('visible');
	}
}

function nextImg(){
	if(imgCnt < 8){
		imgCnt++;
		console.log(imgCnt);
		photo_viewer(1);
	}
}

function prevImg(){
	if(imgCnt > 1){
		imgCnt--;
		console.log(imgCnt);
		photo_viewer(1);
	}
}

function closeImg(){
	photo_viewer(2);
	PageTransitions.goToPage(2,'meetingTimePage');
  	
}

function controlPage(param){
	param = "";
	document.getElementById('user-profile').style.visibility = param;
	document.getElementById('profile-txt').style.visibility = param;
	document.getElementById('section').style.visibility = param;
	document.getElementById('menu').style.visibility = param;
	document.getElementById('notification').style.visibility = param;
	document.getElementById('body-buttons').style.visibility = param;
	document.getElementById('myForm').style.visibility = param;
}