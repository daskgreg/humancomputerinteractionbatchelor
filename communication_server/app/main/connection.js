var remoteMessenger = require('../../services/remoteMessenger/remoteMessenger');
const MESSAGE_TYPES = require('../../services/messageTypes');

exports.sendNextSlide = (message, deviceTypes) => {
    remoteMessenger.sendMessage(MESSAGE_TYPES.NEXTSLIDE, message, deviceTypes);
}

exports.sendPrevSlide = (message, deviceTypes) => {
    remoteMessenger.sendMessage(MESSAGE_TYPES.PREVSLIDE, message, deviceTypes);
}