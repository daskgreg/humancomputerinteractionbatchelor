
var express = require('express');
var controller = require('./controller');

var router = express.Router();

//add your sub-routes (you can think them as remote functions)
router.post('/prevslide', controller.prevslide);
router.post('/nextslide', controller.nextslide);

module.exports = router;