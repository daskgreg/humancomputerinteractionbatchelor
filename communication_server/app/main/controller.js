var connection = require('./connection');

var DEVICES = require("../../services/devices");

// The main code for you remote function
exports.nextslide = (req, res) => {
    connection.sendNextSlide({});
    res.send('i received and sent the message successfully');
};

exports.prevslide = (req, res) => {
    connection.sendPrevSlide({});
    res.send('i received and sent the message successfully');
};